variable "api_id" {
  description = "API Gateway REST api ID"
}

variable "resource_id" {
  description = "API resource ID"
}

variable "allowed_methods" {
  type        = list(string)
  description = "List of allowed HTTP methods. OPTIONS is added by default."
}

variable "allowed_origin" {
  description = "Allowed origin"
  default     = "*"
}

variable "allowed_headers" {
  description = "List of allowed headers. Default headers are always present unless discard_default_headers variable is set to true"
  default     = [
    "Authorization",
    "Content-Type",
    "X-Amz-Date",
    "X-Amz-Security-Token",
    "X-Api-Key"
  ]
}

locals {
  methodOptions  = "OPTIONS"

  methods = join(",", distinct(concat(var.allowed_methods, [local.methodOptions])))
}

