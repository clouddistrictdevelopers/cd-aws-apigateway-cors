## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.12 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_api_gateway_integration.cors_integration](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration) | resource |
| [aws_api_gateway_integration_response.cors_integration_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_integration_response) | resource |
| [aws_api_gateway_method.cors_method](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method) | resource |
| [aws_api_gateway_method_response.cors_method_response](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_method_response) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_allowed_headers"></a> [allowed\_headers](#input\_allowed\_headers) | List of allowed headers. Default headers are always present unless discard\_default\_headers variable is set to true | `list` | <pre>[<br>  "Authorization",<br>  "Content-Type",<br>  "X-Amz-Date",<br>  "X-Amz-Security-Token",<br>  "X-Api-Key"<br>]</pre> | no |
| <a name="input_allowed_methods"></a> [allowed\_methods](#input\_allowed\_methods) | List of allowed HTTP methods. OPTIONS is added by default. | `list(string)` | n/a | yes |
| <a name="input_allowed_origin"></a> [allowed\_origin](#input\_allowed\_origin) | Allowed origin | `string` | `"*"` | no |
| <a name="input_api_id"></a> [api\_id](#input\_api\_id) | API Gateway REST api ID | `any` | n/a | yes |
| <a name="input_resource_id"></a> [resource\_id](#input\_resource\_id) | API resource ID | `any` | n/a | yes |

## Outputs

No outputs.
